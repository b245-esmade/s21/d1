

//ARRAY
	//An array is programming is simply a list/multiple of data; related to each other(data types)
	// Declared using square brackets known as "Array literals"

	/*
	Syntax:
		let/const arrayName = [element0, element1, element2, ...]
	*/
	


	// Longhand
	let studentA = "2020-1923";
	let studentB = "2020-1924";
	let studentC = "2020-1925";
	let studentD = "2020-1926";
	let studentE = "2020-1927";

	// Shorthand using Arrays
	let studentNumbers = ["2020-1923","2020-1924","2020-1925", "2020-1926","2020-1927"]

// METHODS
	// another term for functions associated with object/array and is used to execute statements
	// Example: toLowerCase()

// PROPERTY
	// 
	// Example: variableName.length


// Another example
	const grades = [98, 94,89,90]
	console.log(grades)
	grades[1]=80;



	let computerBrands = ["Acer","Asus","Lenovo","Dell","Mac","Samsung"];
	console.log(computerBrands);

	// Create an array with values from variables
	let city1 = "Tokyo";
	let city2 = "Manila";
	let city3 = "New York";

	let cities =[city1,city2,city3];
	console.log(cities);

// [SECTION] Length Property
	// The .length property allows us to get and set total number of items/elements. Spaces are also counted as characters.
	// can only be used with strings

	console.log(grades.length);
	console.log(typeof grades.length);

	let blankArr = [];
	console.log(blankArr.length);

	let fullName = "Eronna"
	console.log(fullName.length);
	// we cannot edit length in strings only in arrays

	let myTasks = [
		"drink HTML",
		"eat Javascript",
		"inhale CSS",
		"bake SASS"]
	console.log(myTasks);

	myTasks.length = myTasks.length-1
	console.log(myTasks)

	let theBeatles = ["John","Paul","Ringo","George"]
	console.log(theBeatles)

	// Adding element in string length
	theBeatles[theBeatles.length]="Eronna";
	console.log(theBeatles)

// [SECTION] Reading/Accessing elements of Arrays
	// Accessing array elements using indices/index


	let lakersLegends = ["Kobe","Shaq","Lebron","Magic","Westbrook","Kareem"];

	console.log(lakersLegends[1]); //Shaq

	// We can also save/store array elements in another variable

	let  currentLaker = lakersLegends[2]; //Lebron

	console.log("Array before the reassignment");
	lakersLegends[4] = "Pau Gasol";
	console.log("Array before the reassignment");
	console.log(lakersLegends);

	// Accessing the last element of an array
		console.log(lakersLegends[lakersLegends.length-1]);

	// Adding items to an array

		let newArr = [];
		newArr[newArr.length]="Cloud Strife";
		console.log(newArr);
		newArr[newArr.length]="Tifa Lockhart"
		console.log(newArr);
		newArr.length = newArr.length-1;
		console.log(newArr);


// [SECTION] Looping over an array
		// You can use for loop to iterate over all items in an array

		let numberArray = [5,12,30,46,40]
		for(let i=0; i<numberArray.length; i++){
			console.log(numberArray[i]);
		}


		for(let i=0; i<numberArray.length-1; i++){
		
			if (numberArray[i]%5 === 0){
				console.log(numberArray[i]+ " is divisible by 5.")
			}else {
				console.log(numberArray[i]+ " is not divisible by 5.")
			}
		}

// [SECTION] Multidimensional Arrays
		// Useful for strong complex data structures
		// Practical application of this is to help visualize/create real world objects
		//Not always recommended due to complexity

		let chessBoard = [
		['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
		['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
		['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
		['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
		['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
		['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
		['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
		['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8'],
	]
		console.log(chessBoard);
		console.table(chessBoard);

		console.log(chessBoard[0][3]);
		console.log(chessBoard[3]);
		console.log(chessBoard[3][5]);
